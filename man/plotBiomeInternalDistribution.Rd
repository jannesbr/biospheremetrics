% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/MECO.R
\name{plotBiomeInternalDistribution}
\alias{plotBiomeInternalDistribution}
\title{Plot to file distribution of similarity within biomes}
\usage{
plotBiomeInternalDistribution(
  data,
  file,
  biomes_abbrv,
  scale,
  title = "",
  legendtitle = "",
  eps = FALSE,
  palette = NULL
)
}
\arguments{
\item{data}{data object with distibution - as returned by 
calculateWithInBiomeDiffs. dim: c(biomes,bins)}

\item{file}{to write into}

\item{biomes_abbrv}{to mask the focusBiome from}

\item{scale}{scaling factor for distribution. defaults to 1}

\item{title}{character string title for plot, default empty}

\item{legendtitle}{character string legend title, default empty}

\item{eps}{write as eps or png (default: F -> png)}

\item{palette}{color palette to plot EcoRisk with, defaults to the Ostberg 
color scheme white-blue-yellow-red}
}
\value{
None
}
\description{
Function to plot to file the distribution of similarity within biomes
}
\examples{
\dontrun{

}
}
